package gratuitycalculator.becotech.com.gratuitycalculator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.crashlytics.android.Crashlytics;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class MainPage extends AppCompatActivity {

    ImageView im;
    Spinner bu1,b2;
    public static String valuespin,valuespin2;
    Context ctx;
    Button b;
    public static final String QRVIEWPREFS_NAME = "LanguageandCountryData";
    SharedPreferences prefs;
    String sp1, sp2, sp3;
    int si1, si2;
    ArrayAdapter adapter1, adapter2;
    TextView tx;
    public String CountryID;
static public int TAG =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        loadLocale();
        setContentView(R.layout.activity_main_page);

        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        CountryID = tm.getNetworkCountryIso();
//tx=(TextView) findViewById(R.id.txerror);
        im=(ImageView) findViewById(R.id.imageView2);
        bu1=(Spinner) findViewById(R.id.spin1);
        adapter1 = ArrayAdapter.createFromResource(this, R.array.country_arrays, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        bu1.setAdapter(adapter1);
        b2=(Spinner) findViewById(R.id.spin2);
        adapter2 = ArrayAdapter.createFromResource(this, R.array.language_arrays, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        b2.setAdapter(adapter2);
b=(Button) findViewById(R.id.nextbt);

bu1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                valuespin = item.toString();
                SharedPreferences settings = getSharedPreferences(QRVIEWPREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("country", valuespin);
                editor.commit();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        b2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                valuespin2 = item.toString();
                TAG = 1;
                SharedPreferences settings = getSharedPreferences(QRVIEWPREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("language", valuespin2);
                SharedPreferences pre = getSharedPreferences("com", Context.MODE_PRIVATE);
                pre.edit().putInt("co", TAG).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tx.setVisibility(View.VISIBLE);
//
//                if(valuespin.equals("Select Country") && valuespin2.equals("Select Language")){
//
//                    tx.setText("Please select any Language and Country");
//
//
//                }
//                else if(valuespin.equals("Select Country")){
//
//                    tx.setText("Please Select any Country");
//
//
//                }
//                else if(valuespin2.equals("Select Language")){
//
//                    tx.setText("Please select any Language");
//
//
//                }
//
//                else{
//                    tx.setVisibility(View.INVISIBLE);
                   if(valuespin2.equals("English")){

                    setLocale("en");
           //         recreate();
                }

                else if(valuespin2.equals("Arabic"))
                {
                    setLocale("ar");
                //    recreate();
                }
                    Intent i = new Intent(MainPage.this,MainActivity.class);
                    startActivity(i);

//                }
//                else if(valuespin2.equals("English")){
//
//                    setLocale("en");
//                    recreate();
//                }
//
//                else if(valuespin2.equals("Arabic"))
//                {
//                    setLocale("ar");
//                    recreate();
//                }

            }
        });
    }

    private void setLocale(String lang)
    {
        Locale locale= new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale=locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor sp= getSharedPreferences(QRVIEWPREFS_NAME, MODE_PRIVATE).edit();
        sp.putString("My Lang", lang);
        sp.apply();
    }
    public void loadLocale()
    {
        SharedPreferences sharedPreferences= getSharedPreferences(QRVIEWPREFS_NAME, Activity.MODE_PRIVATE);
        String language= sharedPreferences.getString("My Lang","");
        setLocale(language);
    }

    @Override
    protected void onResume() {
        super.onResume();






        Spinner spinner = (Spinner)findViewById(R.id.spin1);


        String a= CountryID;
        SharedPreferences pr= getSharedPreferences("com", Context.MODE_PRIVATE);
        int s1 = pr.getInt("co", 0);
      if (a.equals("in") && s1 == 0){
            spinner.setSelection(2);

        }

        else if (a.equals("ae") && s1 == 0){
            spinner.setSelection(1);

        }
       else {

         SharedPreferences prefs= getSharedPreferences("prefs1", Context.MODE_PRIVATE);
         int si1 = prefs.getInt("countrys", 0);

        spinner.setSelection(si1);
        }

        Spinner spinn = (Spinner)findViewById(R.id.spin2);


//spinn.setOnItemSelectedListener(new OnItemSelectedListener() {
//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        Object item = parent.getItemAtPosition(position);
//        valuespin2 = item.toString();
//        SharedPreferences settings = getSharedPreferences(QRVIEWPREFS_NAME, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putString("language", valuespin2);
//
//
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//
//    }}
//    );

//        try{
//        if(valuespin2.equals("English") || valuespin2.equals(null)){
//
//            setLocale("en");
//            recreate();
//        }
//
//        else if(valuespin2.equals("Arabic"))
//        {
//            setLocale("ar");
//            recreate();
//        }}
//        catch (NullPointerException e)
//        {
//            e.printStackTrace();
//        }
        SharedPreferences pprefs = getSharedPreferences("prefs2", Context.MODE_PRIVATE);
        int si2 = pprefs.getInt("languages", 0);
        spinn.setSelection(si2);

    }

    @Override
    protected void onPause() {
        super.onPause();



        Spinner spinner = (Spinner)findViewById(R.id.spin1);

        SharedPreferences prefs = getSharedPreferences("prefs1", Context.MODE_PRIVATE);
        prefs.edit().putInt("countrys", spinner.getSelectedItemPosition()).apply();

        Spinner spinn = (Spinner)findViewById(R.id.spin2);

        SharedPreferences pprefs = getSharedPreferences("prefs2", Context.MODE_PRIVATE);
        pprefs.edit().putInt("languages", spinn.getSelectedItemPosition()).apply();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Spinner spinner = (Spinner)findViewById(R.id.spin1);

        SharedPreferences prefs = getSharedPreferences("prefs1", Context.MODE_PRIVATE);
        prefs.edit().putInt("countrys", spinner.getSelectedItemPosition()).apply();

        Spinner spinn = (Spinner)findViewById(R.id.spin2);

        SharedPreferences pprefs = getSharedPreferences("prefs2", Context.MODE_PRIVATE);
         pprefs.edit().putInt("languages", spinn.getSelectedItemPosition()).apply();

        SharedPreferences set = getApplicationContext().getSharedPreferences("com", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = set.edit();
        edit.clear();
        edit.commit();
    }
}
