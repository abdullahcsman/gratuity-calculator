package gratuitycalculator.becotech.com.gratuitycalculator;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static gratuitycalculator.becotech.com.gratuitycalculator.MainPage.QRVIEWPREFS_NAME;

public class gc_fragment extends Fragment {

        private EditText editText_start;
    private EditText editText_end;
    private RadioButton radioButton;
    private RadioGroup radioGroup;
    private Button button;
    private View view;
    AdView mAdView;
    private Calendar myCalendar, myCalendar_st;

    TextView textView_result;
    EditText editText_salary;

        DatePickerDialog.OnDateSetListener date, date1;
    SimpleDateFormat start_df, end_df;
SharedPreferences setting;
    AppCompatActivity mActivity;
    int value;
    int progress;
    double totalGratuity = 0.0;
    double perdaySalary = 0.0;
    double serviceYears = 0.0;
    double salary;
    int totalDays;
String la;
String bsalary;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View v= inflater.inflate(R.layout.gc_fragment, container, false);

        mAdView = (AdView) v.findViewById(R.id.adView);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest1);
        editText_start = (EditText) v.findViewById(R.id.editText_start);
        editText_start.requestFocus();
        editText_start.setInputType(InputType.TYPE_NULL);

        editText_end = (EditText) v.findViewById(R.id.editText_end);
        editText_end.setInputType(InputType.TYPE_NULL);

        editText_salary = (EditText) v.findViewById(R.id.editText_salary);

        editText_salary.addTextChangedListener(onTextChangedListener());

        button = (Button) v.findViewById(R.id.button);
        radioGroup = (RadioGroup) v.findViewById(R.id.radioterminate);
        myCalendar_st = Calendar.getInstance();

        myCalendar = Calendar.getInstance();
        textView_result = (TextView) v.findViewById(R.id.result);

        SharedPreferences sharedPreferences= getActivity().getSharedPreferences(QRVIEWPREFS_NAME, Activity.MODE_PRIVATE);
        la= sharedPreferences.getString("My Lang","en");

        setting=getActivity().getSharedPreferences(QRVIEWPREFS_NAME, Context.MODE_PRIVATE);
        bsalary= setting.getString("country", "UAE");


        editText_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(gc_fragment.this.getContext(),android.R.style.Theme_Holo_Light_Panel , date, myCalendar_st
                        .get(Calendar.YEAR), myCalendar_st.get(Calendar.MONTH),
                        myCalendar_st.get(Calendar.DAY_OF_MONTH)).show();
                editText_end.requestFocus();
            }
        });


        editText_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(gc_fragment.this.getContext(),android.R.style.Theme_Holo_Light_Panel, date1, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();


          editText_salary.requestFocus();
            }
        });

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {


                // TODO Auto-generated method stub
                myCalendar_st.set(Calendar.YEAR, year);
                myCalendar_st.set(Calendar.MONTH, monthOfYear);
                myCalendar_st.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateLabel(0);
            }
        };

        date1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(1);
            }

        };



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                textView_result.setVisibility(View.VISIBLE);
                if (editText_start.getText().length() == 0) {

                    textView_result.setText(getResources().getString(R.string.war1));
                } else if (editText_end.getText().length() == 0) {
                    textView_result.setText(getResources().getString(R.string.war2));
                } else if (editText_salary.getText().length() == 0) {

                    textView_result.setText(getResources().getString(R.string.war3));
                } else {
                    {

                        double totalGratuity = 0.0;
                        double perdaySalary = 0.0;
                        double serviceYears = 0.0;
                        long i=0;
                        int selectedId = radioGroup.getCheckedRadioButtonId();

                        radioButton = (RadioButton) v.findViewById(selectedId);

                        if (bsalary.equals("UAE")){
                        double totalDays = (double)get_count_of_days(editText_start.getText().toString(), editText_end.getText().toString());
                        //textView_result.setText("Your Gratuity: "+totalDays);

                        String s = editText_salary.getText().toString();

                        double salary = Double.parseDouble(s.replaceAll(",",""));

                        perdaySalary = (salary * 12.0) / 365.0;
                        serviceYears = totalDays / 365.0;
                        if (radioButton.getText().toString().equalsIgnoreCase(getResources().getString(R.string.rg2name))) {
                            if (serviceYears >= 5) {
                                totalGratuity = (perdaySalary * serviceYears) * 30.0;
                            } else if (serviceYears < 5 && serviceYears >= 3) {
                                totalGratuity = (perdaySalary * serviceYears) * 21.0;
                            } else if (serviceYears < 3 && serviceYears >= 1) {
                                totalGratuity = (perdaySalary * serviceYears) * 21.0;
                            }
                        } else {
                            if (serviceYears >= 5) {
                                totalGratuity = (perdaySalary * serviceYears) * 30.0;
                            } else if (serviceYears < 5 && serviceYears >= 3) {
                                totalGratuity = (perdaySalary * serviceYears) * 14.0;
                            } else if (serviceYears < 3 && serviceYears >= 1) {
                                totalGratuity = (perdaySalary * serviceYears) * 7.0;
                            }
                        }


                        //textView_result.setText(R.string.tg +" " + Math.round(totalGratuity));
//Changing
                         i = Math.round(totalGratuity);

                            if (la.equals("en"))
                            {
                                DecimalFormat myFormatter = new DecimalFormat("#,###");
                                String output = myFormatter.format(i);
                                textView_result.setText(getResources().getString(R.string.tg) + " " + output +  " " + getResources().getString(R.string.cuur1));

                            }
                            else if (la.equals("ar"))
                            {
                                NumberFormat nf=NumberFormat.getInstance(new Locale("ar","EG"));
                                String a=nf.format(i);
                                textView_result.setText(getResources().getString(R.string.tg) + " " + a +  " " + getResources().getString(R.string.cuur1));
                            }
//changing End
                    }
                    else if (bsalary.equals("India"))
                        {

                            long totalDays = get_count_of_years(editText_start.getText().toString(), editText_end.getText().toString());
                            String s = editText_salary.getText().toString();
                            double salary = Double.parseDouble(s.replaceAll(",", ""));
//            Toast.makeText(MainActivity.this, "days: " + totalDays, Toast.LENGTH_SHORT).show();
                            serviceYears = totalDays ;
//
                            if (serviceYears >= 5) {
                                totalGratuity = salary * 15 / 26 * serviceYears;

                                i = Math.round(totalGratuity);
                                // Toast.makeText(Main2Activity.this,"Result" + i, Toast.LENGTH_SHORT).show();;
                                if (la.equals("en"))
                                {
                                    DecimalFormat myFormatter = new DecimalFormat("#,###");
                                    String output = myFormatter.format(i);
                                    textView_result.setText(getResources().getString(R.string.tg) + " " + output +  " " + getResources().getString(R.string.cuur2));}
                                else if (la.equals("ar"))
                                {
                                    NumberFormat nf=NumberFormat.getInstance(new Locale("ar","EG"));
                                    String a=nf.format(i);
                                    textView_result.setText(getResources().getString(R.string.tg) + " " + a +  " " + getResources().getString(R.string.cuur2));
                                }

                            } else if (serviceYears < 5 || totalDays <5 ) {
                                textView_result.setText(getResources().getString(R.string.war4));
                            }
//                            i = Math.round(totalGratuity);
//                            // Toast.makeText(Main2Activity.this,"Result" + i, Toast.LENGTH_SHORT).show();;
//                            if (la.equals("en"))
//                            {
//                                DecimalFormat myFormatter = new DecimalFormat("#,###");
//                                String output = myFormatter.format(i);
//                                textView_result.setText(getResources().getString(R.string.tg) + " " + output +  " " + getResources().getString(R.string.cuur2));}
//                            else if (la.equals("ar"))
//                            {
//                                NumberFormat nf=NumberFormat.getInstance(new Locale("ar","EG"));
//                                String a=nf.format(i);
//                                textView_result.setText(getResources().getString(R.string.tg) + " " + a +  " " + getResources().getString(R.string.cuur2));
//                            }
                        }

                }


            }

            }
        });


        return v;
    }


        //funtion text watch is used////////
    private TextWatcher onTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editText_salary.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    editText_salary.setText(formattedString);
                    editText_salary.setSelection(editText_salary.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                editText_salary.addTextChangedListener(this);
            }
        };
    }


        private void updateLabel(int value) {




        //===========================
        //===========================

//        String myFormat = "dd/MM/yyyy"; //In which you need put here
        String myFormat = "dd MMM, yyyy";

        if (value == 1) {
            end_df = new SimpleDateFormat(myFormat, Locale.US);
            editText_end.setText(end_df.format(myCalendar.getTime()));
        } else {
            start_df = new SimpleDateFormat(myFormat, Locale.US);
            editText_start.setText(start_df.format(myCalendar_st.getTime()));
        }
    }
    public long get_count_of_years(String Created_date_String, String Expire_date_String) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH);

        float result = 0;
        long i = 0;
        float ye = 0;

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);

            long timeOne = Created_convertedDate.getTime();
            long timeTwo = Expire_CovertedDate.getTime();
            long oneDay = 1000 * 60 * 60 * 24;
//            Calendar c= new GregorianCalendar(TimeZone.getTimeZone(Created_date_String));
//            Calendar d= new GregorianCalendar(TimeZone.getTimeZone(Expire_date_String));
//         int yearsInBetween = Expire_date_String.get(Calendar.YEAR) - Created_date_String.get(Calendar.YEAR);
//         int monthsDiff = today.get(Calendar.MONTH) - birthDay.get(Calendar.MONTH);
//            long ageInMonths = yearsInBetween*12 + monthsDiff;
//            long age = yearsInBetween;




            long delta = (timeTwo - timeOne) / oneDay;
            long delt = (timeTwo - timeOne) / 365;
            result = (float) delta;
//            Toast.makeText(MainActivity.this,"daysss: "+result,Toast.LENGTH_SHORT).show();

            ye = result/365;
            i = Math.round(ye);
//            Toast.makeText(MainActivity.this,"da: "+i,Toast.LENGTH_SHORT).show();
//            textView_result.setText("Your Gratuity: " + result + "AED");
//            DecimalFormat myFormatter = new DecimalFormat("#,###");
//
//            String output = myFormatter.format(result);
//
//            textView_result.setText("Your Gratuity: " + output + " AED");

        } catch (ParseException e) {
            e.printStackTrace();
        }
//Toast.makeText(mActivity,"days: "+result,Toast.LENGTH_SHORT).show();
        return i;
    }
    public int get_count_of_days(String Created_date_String, String Expire_date_String) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH);

        int result = 0;

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);

            long timeOne = Created_convertedDate.getTime();
            long timeTwo = Expire_CovertedDate.getTime();
            long oneDay = 1000 * 60 * 60 * 24;

            long delta = (timeTwo - timeOne) / oneDay;
            result = (int) delta;
            textView_result.setText("Your Gratuity: " + result + "AED");
            DecimalFormat myFormatter = new DecimalFormat("#,###");

                      String output = myFormatter.format(result);

                       textView_result.setText("Your Gratuity: " + output + " AED");

        } catch (ParseException e) {
            e.printStackTrace();
        }
//Toast.makeText(mActivity,"days: "+result,Toast.LENGTH_SHORT).show();
        return (int) result;
    }


}
