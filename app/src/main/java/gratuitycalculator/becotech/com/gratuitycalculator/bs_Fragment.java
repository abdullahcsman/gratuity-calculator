package gratuitycalculator.becotech.com.gratuitycalculator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static gratuitycalculator.becotech.com.gratuitycalculator.MainPage.QRVIEWPREFS_NAME;

@SuppressLint("ValidFragment")
public class bs_Fragment extends Fragment {


    AdView mAdView;
    Button btn;
    EditText editText;
    TextView textView_result;
    String bsalary;
    SharedPreferences setting;

String la;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v= inflater.inflate(R.layout.bs_fragment, container, false);

        mAdView = (AdView) v.findViewById(R.id.adView);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest1);



        textView_result = (TextView) v.findViewById(R.id.result);
        editText = (EditText) v.findViewById(R.id.editText_start);
        editText.addTextChangedListener(onTextChangedListener());
        btn = (Button) v.findViewById(R.id.button);

        setting=getActivity().getSharedPreferences(MainPage.QRVIEWPREFS_NAME,Context.MODE_PRIVATE);
        bsalary= setting.getString("country", "UAE");
        SharedPreferences sharedPreferences= getActivity().getSharedPreferences(QRVIEWPREFS_NAME, Activity.MODE_PRIVATE);
        la= sharedPreferences.getString("My Lang","en");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView_result.setVisibility(View.VISIBLE);
                if (editText.getText().length() == 0) {

                    textView_result.setText(R.string.war3);
                }
                else {
//                    double result = 0.0;
                    double result = 0.0;
//                String content = editText.getText().toString();
//                int nIntFromET = Integer.parseInt(content);
                    String s = editText.getText().toString();
                    double salary = Double.parseDouble(s.replaceAll(",",""));
                    result = (salary * 12.0) / 365.0;
//                 result = nIntFromET*12/365;

                    textView_result.setText("Your Gratuity: " + Math.round(result));

                    long i = Math.round(result);

                    DecimalFormat myFormatter = new DecimalFormat("#,###");
                    String output = myFormatter.format(i);
                    if(bsalary.equals("UAE"))
                    {
                    textView_result.setText(getResources().getString(R.string.tb) + " " + output + " " + getResources().getString(R.string.cuur1));}

                    else if(bsalary.equals("India"))
                    {
                        textView_result.setText(getResources().getString(R.string.tb) + " " + output + " " + getResources().getString(R.string.cuur2));}
                }
            }
        });



        return v;
    }

    private TextWatcher onTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editText.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    editText.setText(formattedString);
                    editText.setSelection(editText.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                editText.addTextChangedListener(this);
            }
        };
    }


}
